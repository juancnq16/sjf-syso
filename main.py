from flask import Flask, render_template, request, redirect, Response, make_response, jsonify
import json 
from Administrador import *
from Nodo import *
app = Flask(__name__)
cola = Admin()
@app.route('/<path:the_path>')
def all_other_routes(the_path):
    return app.send_static_file(the_path)
@app.route('/addCliente', methods = ['POST'])
def addCliente():
    datos = str(request.get_data())[2:][:-1]
    print("los datos", request.get_data())
    print ("el json: ", request.get_json())
    datos_json = json.loads(datos)
    cola.add(int(datos_json['pendientes']))
    respuesta = cola.getArreglo()
    respuesta = {
        "lista":respuesta
    }
    respuesta = json.dumps(respuesta)
    return respuesta
@app.route('/atender', methods = ['POST'])
def atender():
    ans = cola.atender()
    #ans = cola.getArreglo()
    ans = {
        "lista":ans
    }
    ans = json.dumps(ans)
    print(ans)
    return ans
@app.route('/consultar', methods = ['POST'])
def consultar():
    respuesta = cola.getArreglo()
    respuesta = {
        "lista":respuesta
    }
    respuesta = json.dumps(respuesta)
    print(respuesta)
    return respuesta
@app.route('/table', methods = ['POST'])
def gant():
    arreglo = cola.liquidar()
    respuesta = {
        "tabla":arreglo
    }
    respuesta = json.dumps(respuesta)
    return respuesta
@app.route('/datos', methods = ['POST'])
def datos():
    datos = cola.getArreglo()
    ans = []
    for dato in datos:
        envio = {
            "id":dato[0],
            "pendientes":dato[1]
        }
        ans.append(envio)
    print(ans)
    return {
        "datos":ans
    }
@app.route('/')
def index():
    return app.send_static_file("index.html")
if __name__ == '__main__':
    app.run(debug=True)
