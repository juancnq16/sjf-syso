class Nodo:
    def __init__(self, pendientes, id):
        self._pendientes = int(pendientes)
        self._siguiente = None
        self._tLlegada = 0
        self._tRafaga = int(pendientes)
        self._tComienzo = 0
        self._tFinal = 0 
        self._tRetorno = 0
        self._tEspera = 0
        self._id=id 
    def liquidar(self, entero):
        print("la rafaga: ", self._tRafaga)
        self._tFinal = entero
        #self._tFinal = self._tRafaga - self._tComienzo
        self._tRetorno = self._tFinal - self._tLlegada
        self._tEspera = self._tRetorno - self._tRafaga
        return {
            "id":self._id,
            "tLllegada":self._tLlegada,
            "tRafaga":self._tRafaga,
            "tComienzo":self._tComienzo,
            "tFinal":self._tFinal,
            "tRetorno":self._tRetorno,
            "tEspera":self._tEspera
        }
    def getId(self):
        return self._id
    def atender(self):
        self._pendientes -= 1
        self.esperar()
    def getSiguiente(self):
        return self._siguiente
    def setSiguiente(self, nodo):
        self._siguiente = nodo
    def getPendientes(self):
        return self._pendientes
    def add(self, nodo):
        if(self._siguiente != None):
            print("ya hay un siguiente")
            if (self._siguiente.getPendientes() < nodo.getPendientes()):
                self._siguiente.add(nodo)
            else:
                nodo.setSiguiente(self._siguiente)
                self._siguiente = nodo
        else:
            print("no hay un siguiente")
            self._siguiente=nodo
    def getArreglo(self, arreglo):
        arreglo.append((self._id,self._pendientes))
        if(self._siguiente!=None):
            return self._siguiente.getArreglo(arreglo)
        else:
            return arreglo
    def esperar(self):
        if self._siguiente != None:
            self._siguiente._tEspera+=1
            self._siguiente.esperar()
    def contar(self):
        return self.contar2(0)
    def contar2(self, entero):
        if self._siguiente == None:
            return entero+1
        else:
            return self._siguiente.contar2(entero+1)

