import { html, PolymerElement } from "./node_modules/@polymer/polymer/polymer-element.js";
import "./node_modules/@polymer/iron-ajax/iron-ajax.js";
import "./node_modules/@polymer/polymer/lib/elements/dom-repeat.js";
/**
 * @customElement
 * @polymer
 */

class DiagramaGantt extends PolymerElement {
  static get template() {
	return html`
		<style>
			:host{
				display:block;
				text-align: center;
			}
			.tabla, .th, .td{
				width:30%;
				padding: 10px 20px;
				display:inline-block;
				vertical-align:top;
				border: 1px solid black;
			}
			.board{
				display:inline-block;
				width:60%;
				border: 1px solid black;
			}
			tr{

				border: 1px solid red;
			}
		</style>
		<iron-ajax
		id="atender"
		url="http://127.0.0.1:5000/atender"
		method="POST"
		handle-as="json"
		on-response="recibe"
		on-error="finaliza"
		></iron-ajax>
		
		<iron-ajax
		id="addCliente"
		url="http://127.0.0.1:5000/addCliente"
		method="POST"
		handle-as="json"
		on-response="recibeAdd"
		on-error="finalizaAdd"
		></iron-ajax>
		
		<iron-ajax
		id="table"
		url="http://127.0.0.1:5000/table"
		method="POST"
		handle-as="json"
		on-response="getTable"
		></iron-ajax>

		<iron-ajax
		id="datos"
		url="http://127.0.0.1:5000/datos"
		method="POST"
		handle-as="json"
		on-response="fillTable"
		></iron-ajax>

		
		<input type="number" placeholder="Rafaga a ejecutar" id="procesos"></input>
		<br>
		<button on-click="addCliente">Añadir cliente</button>
		<br>
		<button on-click="atender">Ejecutar cliente</button>
		<p on-click=drawSquare> Hola Mundo [[prop1]]</p>
		<div style = "display:[[display]]" >
			<table>
				<tr>
					<th>Proceso</th>
					<th>tiempo llegada</th>
					<th>tiempo rafaga</th>
					<th>tiempo comienzo</th>
					<th>tiempo final</th>
					<th>tiempo retorno</th>
					<th>tiempo espera</th>
				</tr>
				<template is="dom-repeat" items=[[datosTabla]] as="fila">
					<tr>
						<td>[[fila.id]]</td>
						<td>[[fila.tLllegada]]</td>
						<td>[[fila.tRafaga]]</td>
						<td>[[fila.tComienzo]]</td>
						<td>[[fila.tFinal]]</td>
						<td>[[fila.tRetorno]]</td>
						<td>[[fila.tEspera]]</td>
					</tr>
			  	</template>
			</table>
		</div>
		<br>
		<table class = "tabla">
			<tr>
				<th class = "th"># Proceso </th>
				<th class = "th">Pendientes</th>
			</tr>
				<template is="dom-repeat" items=[[datos]] as="datos">
					<tr>
						<td class = "td">[[datos.id]]</td>
						<td class = "td">[[datos.pendientes]]</td>
					</tr>
			  	</template>
		</table>
		<canvas id = "canvas" width="600" height="400" class = "board"></canvas>
    `;
  }

  static get properties() {
    return {
      prop1: {
        type: String,
        value: '| 1 | 2 | 3 |'
	  },
	  procesos:{
		  type:Array
	  },
	  gantt:{
		  type:Array
	  },
	  datosTabla:{
		type:Object
	  },
	  display:{
		  type:String,
		  value:"none;"
	  },
	  datos:{
		type:Object
	  }
    };
  }
  recibe(evento, respuesta){
	var datos = respuesta.response;
	var lista = datos["lista"];
	var tabla = datos["tabla"];
	//por codificar
	
	console.log("datos atiende: ", datos);
	this.procesos = lista;
	this.draw();
	this.$.datos.generateRequest();
  }
  fillTable(evento, respuesta){
	respuesta = respuesta.response;
	this.datos = respuesta["datos"];
	console.log("datos despues de llegada",this.datos);
  }
  atender(){
	this.$.atender.generateRequest();
  }
  finaliza(){
	  alert("Si su pila de trabajos no ha finalizado contacte a soporte"+"\n"+"Gracias!");
		this.$.table.generateRequest();
  }
  getTable(evento, respuesta){
	  var datos = respuesta.response;
		console.log(datos);
		this.display="inline-block;"
		this.datosTabla = datos['tabla'];
  }
  addCliente(){
	  var rafaga = this.$.procesos.value;
	  var envio = {"pendientes":rafaga}
	  var envio = JSON.stringify(envio);
		this.$.addCliente.body=envio;
		this.$.addCliente.generateRequest();
  }

  recibeAdd(evento, respuesta){
	var datos = respuesta.response;
	console.log("datos recibe:", datos);
	this.procesos=datos["lista"];
	//this.drawSquare();
  }
  prueba(){
	  	

		// Create gradient
		var grd = ctx.createRadialGradient(75, 50, 5, 90, 60, 100);
		grd.addColorStop(0, "red");
		grd.addColorStop(1, "white");

		// Fill with gradient
		ctx.fillStyle = grd;
		ctx.fillRect(10, 10, 150, 80);
  }
	  draw(){
		var c = this.$.canvas;
		var ctx = c.getContext("2d");
		ctx.beginPath();
		var current = this.procesos.length-1
		for (let i=0; i<this.procesos[current].length; i++){
			if (this.procesos[current][i] == 1){
				ctx.fillStyle = "red";
				ctx.rect(25*current, 25*i, 20, 20);
				ctx.fill();
			}else{
				ctx.fillStyle = "black";
				ctx.rect(25*current, (25*i)+7, 20, 5);
				ctx.fill();
			}
		}
	  }
}

window.customElements.define('diagrama-gantt', DiagramaGantt);