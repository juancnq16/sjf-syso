from Nodo import *
class Admin():
    def __init__(self):
        self._cabeza = None
        self._contador=0
        self._tiempo = 0
        self._eventos = []
        self._procesos = []
        self._historial = []
    def atender(self):
        arreglo = []
        for i in range(0,self._contador):
            if self._cabeza.getId() == i:
                arreglo.append(1)
            else:
                arreglo.append(0)
        self._procesos.append(arreglo)
        if self._cabeza._pendientes == self._cabeza._tRafaga :
            self._cabeza._tComienzo = self._tiempo
        self._cabeza.atender()
        self._tiempo += 1
        if self._cabeza.getPendientes()<=0:
            self._historial.append(self._cabeza.liquidar(self._tiempo))
            self._cabeza = self._cabeza.getSiguiente()
        return self._procesos
    def add(self, pendientes):
        nodo = Nodo(pendientes,self._contador)
        self._historial.append(pendientes)
        nodo._tLlegada = self._tiempo
        self._contador +=1
        if self._cabeza==None:
            self._cabeza=nodo
        elif self._cabeza.getPendientes() > nodo.getPendientes():
            nodo.setSiguiente(self._cabeza)
            self._cabeza = nodo
        else:
            self._cabeza.add(nodo)
    def getArreglo(self):
        return self._cabeza.getArreglo([])
    def getDatos(self):
        indice = 0
        ans = []
        for elem in self._historial:
            ans.append([indice,elem])
            indice +=1
        return ans
    def getGant(self):
        return self._procesos
    def liquidar(self):
        return self._historial